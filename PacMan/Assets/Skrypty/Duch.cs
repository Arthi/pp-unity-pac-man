﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duch : MonoBehaviour {



    public Sprite trybJedzenia;
    public Sprite zjedzony;

    private float oryginalnaPredkoscPoruszania = 3.9f;
    private float obecnaPredkoscPoruszania;

    private float TimerCzasuTrybuJedzenia = 0;

    public Wezel pozycjaStartowa;
    public Wezel mojNaroznik;

    private int rotacjaTrybu;
    private float TimerZmianyTrybu;

    private enum Tryb
    {
        Pogon, Rozpraszanie, Jedzenie, Respawn
    }

    private Tryb aktualnyTryb;
    private Tryb poprzedniTryb;

    private GameObject pacMan;

    private Wezel obecnyWezel;
    private Wezel docelowyWezel;
    private Wezel poprzedniWezel;

    private Vector2 obecnyKierunek;

    public bool czyJestNaRespie;

    public enum KolorDucha
    {
        Czerwony, Rozowy, Niebieski, Pomaranczowy
    }

    public KolorDucha mojKolor;
    public Wezel mojRespawn;
    private GameObject[] duchy = new GameObject[4];
    public Sprite originalSprite;
    private Plansza plansza;


    void Start () {

        inicjalizujDucha();

    }

    void Update()
    {

        zarzadzajTrybami();

        PoruszSie();

        sprawdzKolizje();

        respawnDuchow();

    }

    void inicjalizujDucha() {

        duchy[0] = GameObject.Find("czerwonyDuch");
        duchy[1] = GameObject.Find("niebieskiDuch");
        duchy[2] = GameObject.Find("rozowyDuch");
        duchy[3] = GameObject.Find("pomaranczowyDuch");

        plansza = GameObject.Find("Gra").GetComponent<Plansza>();
        pacMan = GameObject.Find("PacMan");

        PrzygotujDuchyDoStartu();

    }

    void sprawdzKolizje() {

        Rect duch = new Rect(transform.position, transform.GetComponent<SpriteRenderer>().sprite.bounds.size / 2);
        Rect gracz = new Rect(pacMan.transform.position, pacMan.transform.GetComponent<SpriteRenderer>().bounds.size / 2);

        if (duch.Overlaps(gracz))
        {

            if (aktualnyTryb == Tryb.Jedzenie)
            {
                transform.GetComponent<SpriteRenderer>().sprite = zjedzony;
                zmienTrybIPredkosc(Tryb.Respawn);

                plansza.dodajDoWyniku(200); //za ducha dajemy 200

            }
            else
            {

                if (aktualnyTryb != Tryb.Respawn) plansza.Restart();

            }

        };

    }

    void zmienTrybIPredkosc(Tryb tryb)
    {
        aktualnyTryb = tryb;

        //predkosc ducha gdy jest niebieski = 1.9f
        //predkosc ducha gdy jest samymi oczami = 10f

        obecnaPredkoscPoruszania = (tryb == Tryb.Jedzenie ? 1.9f : tryb == Tryb.Respawn ? 10.0f : oryginalnaPredkoscPoruszania);

    }

    public void WlaczTrybZjadania()
    {

        if (aktualnyTryb != Tryb.Respawn) // jesli jest oczami to nie przemieniamy go
        {

            if (TimerCzasuTrybuJedzenia > 0)
            {

                TimerCzasuTrybuJedzenia = 0; //odnawiamy czas

            }
            else
            {

                transform.GetComponent<SpriteRenderer>().sprite = trybJedzenia;

                poprzedniTryb = aktualnyTryb;

                zmienTrybIPredkosc(Tryb.Jedzenie);
            }
        }

    }

    void PoruszSie() {

        if(docelowyWezel != obecnyWezel && docelowyWezel != null && !czyJestNaRespie)
        {

            if (WezelDocelowyOsiagniety())
            {
                obecnyWezel = docelowyWezel;
                transform.localPosition = obecnyWezel.transform.position;

                GameObject portalDocelowy = UstalPortalDocelowy(obecnyWezel.transform.position);

                if (portalDocelowy != null)
                {

                    transform.localPosition = portalDocelowy.transform.position;
                    obecnyWezel = portalDocelowy.GetComponent<Wezel>();

                }

                docelowyWezel = WyznaczNastepnyCel();

                poprzedniWezel = obecnyWezel;

                obecnyWezel = null;

            }
            else
            {

                transform.localPosition += (Vector3)(obecnyKierunek * obecnaPredkoscPoruszania * Time.deltaTime);

            }

        }

    }

    void zarzadzajTrybami() {

        if (aktualnyTryb == Tryb.Jedzenie)
        {

            TimerCzasuTrybuJedzenia += Time.deltaTime;

            if (TimerCzasuTrybuJedzenia > 10) // czas przez jaki duchy sa niebieskie po zjedzeniu energizera
            {

                duchy[0].GetComponent<SpriteRenderer>().sprite = duchy[0].GetComponent<Duch>().originalSprite;
                duchy[1].GetComponent<SpriteRenderer>().sprite = duchy[1].GetComponent<Duch>().originalSprite;
                duchy[2].GetComponent<SpriteRenderer>().sprite = duchy[2].GetComponent<Duch>().originalSprite;
                duchy[3].GetComponent<SpriteRenderer>().sprite = duchy[3].GetComponent<Duch>().originalSprite;

                zmienTrybIPredkosc(poprzedniTryb);

                TimerCzasuTrybuJedzenia = 0;

            }

        }
        else
        {

            TimerZmianyTrybu += Time.deltaTime;

            switch (rotacjaTrybu)
            {
                case 1:
                {
                    zarzadzajTrybamiRotacjiPierwszej();
                    break;
                }
                case 2:
                {
                    zarzadzajTrybamiRotacjiDrugiej();
                    break;
                }
                case 3:
                {
                    zarzadzajTrybamiRotacjiTrzeciej();
                    break;
                }
                case 4:
                {
                    zarzadzajTrybamiRotacjiCzwartej();
                    break;
                }
            }

        }

    }

    private void zarzadzajTrybamiRotacjiPierwszej() {

        if (aktualnyTryb == Tryb.Rozpraszanie && TimerZmianyTrybu > 7)
        {

            zmienTrybIPredkosc(Tryb.Pogon);
            TimerZmianyTrybu = 0;

        }

        if (aktualnyTryb == Tryb.Pogon && TimerZmianyTrybu > 20)
        {

            rotacjaTrybu = 2;
            zmienTrybIPredkosc(Tryb.Rozpraszanie);
            TimerZmianyTrybu = 0;

        }

    }

    private void zarzadzajTrybamiRotacjiDrugiej()
    {

        if (aktualnyTryb == Tryb.Rozpraszanie && TimerZmianyTrybu > 7)
        {

            zmienTrybIPredkosc(Tryb.Pogon);
            TimerZmianyTrybu = 0;

        }

        if (aktualnyTryb == Tryb.Pogon && TimerZmianyTrybu > 20)
        {

            rotacjaTrybu = 3;
            zmienTrybIPredkosc(Tryb.Rozpraszanie);
            TimerZmianyTrybu = 0;

        }

    }

    private void zarzadzajTrybamiRotacjiTrzeciej()
    {

        if (aktualnyTryb == Tryb.Rozpraszanie && TimerZmianyTrybu > 5)
        {

            zmienTrybIPredkosc(Tryb.Pogon);
            TimerZmianyTrybu = 0;

        }

        if (aktualnyTryb == Tryb.Pogon && TimerZmianyTrybu > 20)
        {

            rotacjaTrybu = 3;
            zmienTrybIPredkosc(Tryb.Rozpraszanie);
            TimerZmianyTrybu = 0;

        }

    }

    private void zarzadzajTrybamiRotacjiCzwartej()
    {

        if (aktualnyTryb == Tryb.Rozpraszanie && TimerZmianyTrybu > 5)
        {

            zmienTrybIPredkosc(Tryb.Pogon);
            TimerZmianyTrybu = 0;

        }

        if (aktualnyTryb == Tryb.Pogon && TimerZmianyTrybu > 20)
        {

            zmienTrybIPredkosc(Tryb.Rozpraszanie);
            TimerZmianyTrybu = 0;

        }

    }

    GameObject UstalPortalDocelowy(Vector2 position)
    {
        GameObject tile = plansza.plansza[(int)position.x, (int)position.y];

        if (tile != null)
        {

            if (tile.GetComponent<Obiekt>() != null)
            {

                if (tile.GetComponent<Obiekt>().jestPortalem) return tile.GetComponent<Obiekt>().portalDocelowy;
                
            }
        }

        return null;

    }

    bool WezelDocelowyOsiagniety()
    {

        float doPrzebycia = ((Vector2) docelowyWezel.transform.position - (Vector2) poprzedniWezel.transform.position).sqrMagnitude;
        float przebyte = ((Vector2)transform.localPosition - (Vector2)poprzedniWezel.transform.position).sqrMagnitude;

        return przebyte > doPrzebycia;

    }

    float obliczOdleglosc(Vector2 punktA, Vector2 punktB)
    {
        float odlegloscX = punktA.x - punktB.x;
        float odlegloscY = punktA.y - punktB.y;

        return Mathf.Sqrt(odlegloscX * odlegloscX + odlegloscY * odlegloscY);

    }

    Vector2 PobierzCelPogoniWZaleznosciOdKoloru()
    {

        if (mojKolor == KolorDucha.Czerwony) return duchy[0].GetComponent<czerwonyDuch>().PobierzCel();
        
        else if (mojKolor == KolorDucha.Rozowy) return duchy[2].GetComponent<rozowyDuch>().PobierzCel();

        else if (mojKolor == KolorDucha.Niebieski) return duchy[1].GetComponent<niebieskiDuch>().PobierzCel();

        else if (mojKolor == KolorDucha.Pomaranczowy) return duchy[3].GetComponent<pomaranczowyDuch>().PobierzCel();

        return Vector2.zero;

    }

    Vector2 PobierzCelDuchaWZaleznosciOdTrybu()
    {

        if (aktualnyTryb == Tryb.Pogon)
        {

            return PobierzCelPogoniWZaleznosciOdKoloru();

        }
        else if (aktualnyTryb == Tryb.Rozpraszanie)
        {

            return mojNaroznik.transform.position;

        }
        else if (aktualnyTryb == Tryb.Jedzenie)
        {
            //losowo
            int x = Random.Range(0, 28);
            int y = Random.Range(0, 31);

            return new Vector2(x, y);

        }
        else if (aktualnyTryb == Tryb.Respawn)
        {

            return mojRespawn.transform.position;

        }

        return Vector2.zero;

    }

    Wezel WyznaczNastepnyCel() {

        Vector2 celDucha = PobierzCelDuchaWZaleznosciOdTrybu();
        
        Wezel[] mozliwePunkty = new Wezel[4];

        Vector2[] dozwoloneKierunki = new Vector2[4];

        int counter = 0;

        bool allow = false;
        for (int idx = 0; idx < obecnyWezel.najblizszeWezly.Length; idx++)
        {

            allow = false;

            if (obecnyWezel.dozwoloneKierunki[idx] != obecnyKierunek * -1) //duchom nie wolno zawracać w miejscu
            {

                if (aktualnyTryb == Tryb.Respawn)
                {

                    allow = true;
                   
                }
                else
                {

                    GameObject element = pobierzElementZPozycji(obecnyWezel.transform.position);

                    if (element != null && element.transform.GetComponent<Obiekt>() != null)
                    {

                        if (element.transform.GetComponent<Obiekt>().wejscieDlaDuchow == true)
                        {

                            if (obecnyWezel.dozwoloneKierunki[idx] != Vector2.down) allow = true;
 
                        }

                        else allow = true;
                        
                    }

                    else allow = true;
                    
                }

                if (allow)
                {
                    mozliwePunkty[counter] = obecnyWezel.najblizszeWezly[idx];
                    dozwoloneKierunki[counter] = obecnyWezel.dozwoloneKierunki[idx];
                    counter++;
                }
                
            }
        }

        Wezel punktDocelowy = null;

        if (mozliwePunkty.Length == 1)
        {

            punktDocelowy = mozliwePunkty[0];
            obecnyKierunek = dozwoloneKierunki[0];

        }

        if (mozliwePunkty.Length > 1)
        {

            float najblizszy = 100000;

            for(int i  =0; i < mozliwePunkty.Length; i++)
            {

                if (dozwoloneKierunki[i] != Vector2.zero)
                {

                    float odleglosc = obliczOdleglosc(mozliwePunkty[i].transform.position, celDucha);

                    if (odleglosc < najblizszy)
                    {
                        najblizszy = odleglosc;
                        punktDocelowy = mozliwePunkty[i];
                        obecnyKierunek = dozwoloneKierunki[i];
                    }

                }

            }

        }

        return punktDocelowy;

    }

    GameObject pobierzElementZPozycji (Vector2 pos)
    {

        return plansza.plansza[(int)pos.x, (int)pos.y];

    }

    void respawnDuchow() {

        if (aktualnyTryb == Tryb.Respawn)
        {
            GameObject pole = pobierzElementZPozycji(transform.position);

            if (pole != null)
            {
                if (pole.transform.GetComponent<Obiekt>() != null)
                {
                    if (pole.transform.GetComponent<Obiekt>().startDlaDuchow)
                    {

                        GameObject o = plansza.plansza[(int)transform.localPosition.x, (int)transform.localPosition.y];

                        if (o != null)
                        {

                            Wezel w = o.GetComponent<Wezel>();

                            if (w != null)
                            {

                                obecnyWezel = w;
                                obecnyKierunek = Vector2.up;
                                docelowyWezel = obecnyWezel.najblizszeWezly[0];

                                poprzedniWezel = obecnyWezel;
                                
                                if (mojKolor == KolorDucha.Niebieski) duchy[1].GetComponent<SpriteRenderer>().sprite = duchy[1].GetComponent<Duch>().originalSprite;
                                else if (mojKolor == KolorDucha.Czerwony) duchy[0].GetComponent<SpriteRenderer>().sprite = duchy[0].GetComponent<Duch>().originalSprite;
                                else if (mojKolor == KolorDucha.Pomaranczowy) duchy[3].GetComponent<SpriteRenderer>().sprite = duchy[3].GetComponent<Duch>().originalSprite;
                                else if (mojKolor == KolorDucha.Rozowy) duchy[2].GetComponent<SpriteRenderer>().sprite = duchy[2].GetComponent<Duch>().originalSprite;
                                
                                zmienTrybIPredkosc(Tryb.Pogon);

                                TimerCzasuTrybuJedzenia = 0;

                            }
                        }
                    }
                }
            }
        }

    }

    public void Restart() {

        transform.position = pozycjaStartowa.transform.position;

        GameObject.Find("niebieskiDuch").GetComponent<niebieskiDuch>().Restart();
        GameObject.Find("rozowyDuch").GetComponent<rozowyDuch>().Restart();
        GameObject.Find("pomaranczowyDuch").GetComponent<pomaranczowyDuch>().Restart();

        PrzygotujDuchyDoStartu();

    }

    private void PrzygotujDuchyDoStartu() {

        rotacjaTrybu = 1;
        TimerZmianyTrybu = 0;
        TimerCzasuTrybuJedzenia = 0;

        zmienTrybIPredkosc(Tryb.Rozpraszanie);

        if (transform.name != "czerwonyDuch") czyJestNaRespie = true;

        obecnyWezel = pozycjaStartowa;

        if (czyJestNaRespie)
        {
            obecnyKierunek = Vector2.up;
            docelowyWezel = obecnyWezel.najblizszeWezly[0];
        }
        else
        {
            obecnyKierunek = Vector2.right;
            docelowyWezel = WyznaczNastepnyCel();
        }

        poprzedniWezel = obecnyWezel;

        duchy[0].GetComponent<SpriteRenderer>().sprite = duchy[0].GetComponent<Duch>().originalSprite;
        duchy[1].GetComponent<SpriteRenderer>().sprite = duchy[1].GetComponent<Duch>().originalSprite;
        duchy[2].GetComponent<SpriteRenderer>().sprite = duchy[2].GetComponent<Duch>().originalSprite;
        duchy[3].GetComponent<SpriteRenderer>().sprite = duchy[3].GetComponent<Duch>().originalSprite;

    }



    



}
