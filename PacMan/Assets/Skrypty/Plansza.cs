﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class Plansza : MonoBehaviour {

    public GameObject[] zycia;
    public GameObject[,] plansza = new GameObject[szerokosc, wysokosc];

    private int wszystkiePunkty = 0;
    private int wynik = 0;
    private int pozostaleZycia = 3;
    private static int szerokosc = 28;
    private static int wysokosc = 31;
    private string[] wykluczoneNazwy = {

        "pomaranczowyNaroznik",
        "niebieskiNaroznik",
        "czerwonyNaroznik",
        "rozowyNaroznik",
        "czerwonyDuch",
        "rozowyDuch",
        "niebieskiDuch",
        "pomaranczowyDuch",

    };

	void Start () {

        inicjalizacjaPlanszy();

    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene("Menu");
    }

    public int getLiczbaWszystkichPunktow() {

        return wszystkiePunkty;

    }

    public string pobierzAktualnyWynik() {

        return wynik.ToString();

    }

    public void dodajDoWyniku(int wartosc)
    {

        wynik += wartosc;

    }

    void inicjalizacjaPlanszy() {

        Object[] objects = FindObjectsOfType(typeof(GameObject));

        foreach (GameObject o in objects)
        {
            Vector2 pos = o.transform.position;

            if (!wykluczoneNazwy.Contains(o.name) && o.tag != "Wykluczony")
            {
                Obiekt ob = o.GetComponent<Obiekt>();
                if (ob != null)
                {
                    if (ob.czyKulka || ob.czySuperKulka) wszystkiePunkty++;
                }

                plansza[(int)pos.x, (int)pos.y] = o;
            }

        }

    }

    public void Restart() {

        pozostaleZycia--;

        zycia[pozostaleZycia].SetActive(false);

        if (pozostaleZycia > 0)
        {

            GameObject.Find("PacMan").GetComponent<PacMan>().Restart();

            Object[] objects = GameObject.FindGameObjectsWithTag("Duch");

            foreach (GameObject o in objects)
            {

                o.GetComponent<Duch>().Restart();

            }
            
        }
        else
        {

            SceneManager.LoadScene("Lose");

        }

    }

    public void Win() {

        SceneManager.LoadScene("Win");
    }

}
