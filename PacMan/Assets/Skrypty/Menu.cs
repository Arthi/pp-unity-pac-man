﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    private int pozycja = 0;

    private Image[] elementy = new Image[3];

    public Image zagraj;
    public Image autor;
    public Image wyjscie;

    public Image selector;

	// Use this for initialization
	void Start () {

        elementy[0] = zagraj;
        elementy[1] = autor;
        elementy[2] = wyjscie;

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (pozycja > 0) pozycja--;
            selector.transform.localPosition = new Vector3(selector.transform.localPosition.x, elementy[pozycja].transform.localPosition.y, 0);

        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            if (pozycja < 2) pozycja++;
            selector.transform.localPosition = new Vector3(selector.transform.localPosition.x, elementy[pozycja].transform.localPosition.y, 0);

        }
        else if (Input.GetKeyUp(KeyCode.Return))
        {

            if (pozycja == 0) SceneManager.LoadScene("Pacman");
            if (pozycja == 1) SceneManager.LoadScene("Autor");
            if (pozycja == 2) Application.Quit();

        }

    }





}
