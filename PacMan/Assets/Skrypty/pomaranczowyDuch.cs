﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pomaranczowyDuch : MonoBehaviour {

    private float timerWypuszczeniaDucha = 0;
    public int wypuscMniePo = 21;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timerWypuszczeniaDucha += Time.deltaTime;

        if (timerWypuszczeniaDucha > wypuscMniePo) WypuscMnie();

    }

    void WypuscMnie()
    {

        Duch pomaranczowy = GameObject.Find("pomaranczowyDuch").GetComponent<Duch>();

        if (pomaranczowy.czyJestNaRespie)
        {
            pomaranczowy.czyJestNaRespie = false;
        }

    }

    public void Restart()
    {

        timerWypuszczeniaDucha = 0;

    }

    public Vector2 PobierzCel()
    {

        //obliczamy odległośc od pacmana
        //jeśli dystans jest większy niż 8 pól celujemy bezposrednio w pacmana
        //w przeciwnym razie idizemy do naszego naroznika 

        Vector2 pacManPosition = GameObject.Find("PacMan").transform.localPosition;

        float distance = obliczOdleglosc(transform.localPosition, pacManPosition);

        Vector2 targetTile = Vector2.zero;

        if (distance > 8)
        {

            targetTile = new Vector2((int)pacManPosition.x, (int)pacManPosition.y);

        }
        else
        {

            targetTile = GameObject.Find("pomaranczowyDuch").GetComponent<Duch>().mojNaroznik.transform.position;

        }

        return targetTile;

    }

    float obliczOdleglosc(Vector2 punktA, Vector2 punktB)
    {
        float odlegloscX = punktA.x - punktB.x;
        float odlegloscY = punktA.y - punktB.y;

        return Mathf.Sqrt(odlegloscX * odlegloscX + odlegloscY * odlegloscY);

    }

}
