﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rozowyDuch : MonoBehaviour {

    private float timerWypuszczeniaDucha = 0;
    public int wypuscMniePo = 5;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timerWypuszczeniaDucha += Time.deltaTime;

        if (timerWypuszczeniaDucha > wypuscMniePo) WypuscMnie();

    }

    void WypuscMnie()
    {

        Duch rozowy = GameObject.Find("rozowyDuch").GetComponent<Duch>();

        if (rozowy.czyJestNaRespie)
        {
            rozowy.czyJestNaRespie = false;
        }

    }

    public void Restart()
    {

        timerWypuszczeniaDucha = 0;

    }

    public Vector2 PobierzCel()
    {

        //cztery punkty przed pacmanem

        Vector2 pacManPosition = GameObject.Find("PacMan").transform.localPosition;
        Vector2 pacManOrientation = GameObject.Find("PacMan").GetComponent<PacMan>().orientacja;

        Vector2 pacManTile = new Vector2((int)pacManPosition.x, (int)pacManPosition.y);

        Vector2 targetTile = pacManTile + (4 * pacManOrientation);

        return targetTile;

    }

}
