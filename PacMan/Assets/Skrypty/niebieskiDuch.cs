﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class niebieskiDuch : MonoBehaviour {

    private float timerWypuszczeniaDucha = 0;
    public int wypuscMniePo = 14;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timerWypuszczeniaDucha += Time.deltaTime;

        if (timerWypuszczeniaDucha > wypuscMniePo) WypuscMnie();

    }

    void WypuscMnie()
    {

        Duch niebieski = GameObject.Find("niebieskiDuch").GetComponent<Duch>();

        if (niebieski.czyJestNaRespie)
        {
            niebieski.czyJestNaRespie = false;
        }

    }

    public void Restart() {

        timerWypuszczeniaDucha = 0;

    }

    public Vector2 PobierzCel()
    {
        //dwa punkty przed pacmanem
        //obliczamy odległość
        //wyznaczamy wektor
        //liczymy jego długość
        //podwajamy
        //wyznaczmy punkt docelowy

        Vector2 pacManPosition = GameObject.Find("PacMan").transform.localPosition;
        Vector2 pacManOrientation = GameObject.Find("PacMan").GetComponent<PacMan>().orientacja;

        Vector2 pacManTile = new Vector2((int)pacManPosition.x, (int)pacManPosition.y);

        Vector2 targetTile = pacManTile + (4 * pacManOrientation);

        Vector2 _tmpBluePos = GameObject.Find("niebieskiDuch").transform.localPosition;

        _tmpBluePos = new Vector2((int)_tmpBluePos.x, (int)_tmpBluePos.y);

        float distance = 2 * obliczOdleglosc(_tmpBluePos, targetTile);

        targetTile = new Vector2(_tmpBluePos.x + distance, _tmpBluePos.y + distance);

        return targetTile;

    }

    float obliczOdleglosc(Vector2 punktA, Vector2 punktB)
    {
        float odlegloscX = punktA.x - punktB.x;
        float odlegloscY = punktA.y - punktB.y;

        return Mathf.Sqrt(odlegloscX * odlegloscX + odlegloscY * odlegloscY);

    }
}
