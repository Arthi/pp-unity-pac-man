﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PacMan : MonoBehaviour {

    //Ustawienia PacMana

    public Wezel punktStartowy;
    public Vector2 orientacja = Vector2.right;
    private Vector2 obecnyKierunek = Vector2.right;
    private Vector2 nastepnyKierunek;
    public float predkosc = 4.0f;

    //Dźwięk zjadania przez PacMana kulki

    public AudioClip jedzenie;

    //Pozycja PacMana

    private Wezel obecnyWezel;
    private Wezel wezelDocelowy;
    private Wezel wezelPoprzedni;
    
    //Zjedzone kropki

    private int zezarteKulki = 0;

    private Plansza plansza;

    
    void Start () {

        plansza = GameObject.Find("Gra").GetComponent<Plansza>();

        Wezel wezel = pobierzWezelOPozycji(transform.localPosition);

        obecnyWezel = wezel ?? obecnyWezel;

        ustawKierunekPoruszania(obecnyKierunek);
		
	}
	
	void Update () {

        obslugaKlawiszy();

        PoruszSie();

        obrocPacMana();

        zjedzKropke();
        
    }

    void PoruszSie ()
    {

        if (wezelDocelowy != obecnyWezel && wezelDocelowy != null) {

            if (nastepnyKierunek == (obecnyKierunek * -1))
            {
                obecnyKierunek *= -1;

                Wezel _tmp = wezelDocelowy;
                wezelDocelowy = wezelPoprzedni;
                wezelPoprzedni = _tmp;
            }

            if (WezelDocelowyOsiagniety())
            {

                obecnyWezel = wezelDocelowy;

                transform.localPosition = obecnyWezel.transform.position;

                GameObject portalDocelowy = UstalPortalDocelowy(obecnyWezel.transform.position);

                if (portalDocelowy != null) //teleportacja
                {
                    
                    transform.localPosition = portalDocelowy.transform.position;

                    obecnyWezel = portalDocelowy.GetComponent<Wezel>();

                }

                Wezel docelowyWezel = czyWolnoTamIsc(nastepnyKierunek);

                if (docelowyWezel != null) //wolno więc idziemy
                {
                    obecnyKierunek = nastepnyKierunek;
                }
                else
                {
                    docelowyWezel = czyWolnoTamIsc(obecnyKierunek);
                }

                if (docelowyWezel != null)
                {
                    wezelDocelowy = docelowyWezel;
                    wezelPoprzedni = obecnyWezel;
                    obecnyWezel = null;
                }
                else
                {

                    obecnyKierunek = Vector2.zero; // to co wolno jak nic nie wolno

                }

            }
            else
            {

                transform.localPosition += (Vector3)(obecnyKierunek * predkosc) * Time.deltaTime;

            }

        }
        
    }

    void obslugaKlawiszy() {

        if (Input.GetKeyDown(KeyCode.LeftArrow)) ustawKierunekPoruszania(Vector2.left);
        else if (Input.GetKeyDown(KeyCode.RightArrow)) ustawKierunekPoruszania(Vector2.right);
        else if (Input.GetKeyDown(KeyCode.UpArrow)) ustawKierunekPoruszania(Vector2.up);
        else if (Input.GetKeyDown(KeyCode.DownArrow)) ustawKierunekPoruszania(Vector2.down);

    }

    void obrocPacMana() {

        if (obecnyKierunek == Vector2.right)
        {

            orientacja = Vector2.right;
            transform.localRotation = Quaternion.Euler(0, 0, 0);

        }
        else if (obecnyKierunek == Vector2.up)
        {

            orientacja = Vector2.up;
            transform.localRotation = Quaternion.Euler(0, 0, 90);

        }
        else if (obecnyKierunek == Vector2.left) {

            orientacja = Vector2.left;
            transform.localRotation = Quaternion.Euler(0, 0, 180);

        }
        else if (obecnyKierunek == Vector2.down)
        {

            orientacja = Vector2.down;
            transform.localRotation = Quaternion.Euler(0, 0, 270);

        }

    }

    Wezel pobierzWezelOPozycji(Vector2 pozycja)
    {
        GameObject pole = plansza.plansza[(int)pozycja.x, (int)pozycja.y];

        if (pole != null) return pole.GetComponent<Wezel>();
        
        return null;
    }

    Wezel czyWolnoTamIsc(Vector2 kierunek)
    {

        for(int idx = 0; idx < obecnyWezel.najblizszeWezly.Length; idx++)
        {
            
            if (obecnyWezel.dozwoloneKierunki[idx] == kierunek)
            {

                return obecnyWezel.najblizszeWezly[idx];

            }

        }

        return null;

    }
    void idzDoWezla(Vector2 kierunek)
    {

        Wezel docelowyWezel = czyWolnoTamIsc(kierunek);

        if (docelowyWezel != null)
        {
            transform.localPosition = docelowyWezel.transform.position;
            obecnyWezel = docelowyWezel;
        }

    }

    void ustawKierunekPoruszania(Vector2 kierunek)
    {
        if (kierunek != obecnyKierunek) nastepnyKierunek = kierunek;

        if (obecnyWezel != null)
        {
            Wezel docelowyWezel = czyWolnoTamIsc(kierunek);

            if (docelowyWezel != null)
            {
                obecnyKierunek = kierunek;
                wezelDocelowy = docelowyWezel;
                wezelPoprzedni = obecnyWezel;
                obecnyWezel = null;

            }
        }
    }

    bool WezelDocelowyOsiagniety() {

        float doPrzebycia = ((Vector2) wezelDocelowy.transform.position - (Vector2) wezelPoprzedni.transform.position).sqrMagnitude;
        float przebyte = ((Vector2)transform.localPosition - (Vector2)wezelPoprzedni.transform.position).sqrMagnitude;
        return przebyte > doPrzebycia;

    }

    GameObject UstalPortalDocelowy (Vector2 pozycja)
    {
        GameObject pole = plansza.plansza[(int)pozycja.x, (int)pozycja.y];

        if (pole != null) {

            Obiekt ob = pole.GetComponent<Obiekt>();

            if (ob != null)
            {

                if (ob.jestPortalem) return ob.portalDocelowy;

            }
        }

        return null;

    }

    void zjedzKropke() {

        GameObject o = plansza.plansza[(int)transform.position.x, (int)transform.position.y];

        if (o != null)
        {

            Obiekt ob = o.GetComponent<Obiekt>();

            if (ob != null)
            {

                if (!ob.czyZjedzony && (ob.czyKulka || ob.czySuperKulka))
                {

                    ob.GetComponent<SpriteRenderer>().enabled = false;
                    ob.czyZjedzony = true;

                    plansza.dodajDoWyniku(10); //10 za zwykly

                    if (ob.czySuperKulka)
                    {
                        GameObject[] ghosts = GameObject.FindGameObjectsWithTag("Duch");

                        foreach (GameObject go in ghosts)
                        {
                            go.GetComponent<Duch>().WlaczTrybZjadania();
                        }

                        plansza.dodajDoWyniku(10); // 20 za duzy wiec dodatkowe 10

                    }

                    zezarteKulki++; //zliczamy by sprawdzic kiedy wygralismy

                    transform.GetComponent<AudioSource>().PlayOneShot(jedzenie);

                    GameObject.Find("punkty").GetComponent<Text>().text = plansza.pobierzAktualnyWynik(); //update naszego textu

                    if (zezarteKulki >= plansza.getLiczbaWszystkichPunktow()) // wszystkie więc koniec gry
                    {
                        plansza.Win();
                    }

                }


            }

        }

    }

    public void Restart() {

        transform.position = punktStartowy.transform.position;
        obecnyWezel = punktStartowy;
        obecnyKierunek = Vector2.right;
        orientacja = Vector2.right;
        nastepnyKierunek = Vector2.right;
        ustawKierunekPoruszania(obecnyKierunek);

    }


}
